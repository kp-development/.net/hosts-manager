﻿Imports System.IO
Imports System.Data.SqlServerCe
Imports System.Data
Imports System.Threading.Tasks

Class MainWindow

    Private _HostsList As New List(Of HostList)
    Private _HostFilePath As String

    Private Sub LHC_DG_RowEditEnding(sender As System.Object, e As System.Windows.Controls.DataGridRowEditEndingEventArgs)
        If e.EditAction = DataGridEditAction.Commit Then
            Dim _I1 As FrameworkElement = LHC_DG.Columns(0).GetCellContent(e.Row)
            Dim _I2 As FrameworkElement = LHC_DG.Columns(1).GetCellContent(e.Row)
            Dim _I3 As FrameworkElement = LHC_DG.Columns(2).GetCellContent(e.Row)
            Dim _ID As Long = TryCast(_I1, TextBlock).Text
            Dim _IP As String = TryCast(_I2, TextBlock).Text
            Dim _HOSTS As String = TryCast(_I3, TextBlock).Text
            If _ID <= 0 Then
                'insert
                InsertInto(_IP, _HOSTS)
            Else
                'update
                UpdateRow(_ID, _IP, _HOSTS)
            End If
        End If
    End Sub

    Private Sub LHC_DG_PreviewKeyDown(sender As System.Object, e As System.Windows.Input.KeyEventArgs)
        If e.Key = Key.Delete OrElse e.Key = Key.Back Then
            Dim _DG As DataGrid = sender, _rCt As Long = 0
            If MessageBox.Show("Are you sure you wish to remove these host items?", "Delete", MessageBoxButton.OKCancel) Then
                For Each row In _DG.SelectedItems
                    If row IsNot Nothing Then
                        _rCt += 1
                        DeleteRow(row.ID)
                    End If
                Next
                MsgBox(_rCt & " Items Removed")
            End If
        End If
    End Sub

    Private Sub MainWindow_Loaded(sender As Object, e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        Using New WaitCursor
            Dim _Data As IEnumerable(Of Typing) = Wrapper.GetResults(Of Typing)("Select ID, IPAddress, Hosts From LocalHostCache")
            Me.LHC_DG.ItemsSource = _Data
        End Using
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As System.Windows.RoutedEventArgs) Handles btnLoad.Click
        Dim _FB As New Microsoft.Win32.OpenFileDialog, _LineList = Nothing
        With _FB
            .InitialDirectory = "C:\Windows\System32\drivers\etc" ' Environment.SpecialFolder.SystemX86
            .Title = "Open Your Hosts File"
            If .ShowDialog Then
                Dim _File As String = .FileName, _ct As Long = 0
                _HostFilePath = _File
                _LineList = TryCast(IO.File.ReadLines(_File), IEnumerable)
            End If
        End With
        If _LineList IsNot Nothing Then
            For Each _line In _LineList
                Dim _Rec = If(_line IsNot Nothing, _line.Split(" "), Nothing), _rCt As Long = 0
                If _Rec IsNot Nothing Then
                    _rCt = _Rec.Length
                    Dim _hString As String = String.Empty
                    Dim _IP As String = _Rec(0)
                    For h As Long = 1 To _rCt - 1
                        _hString += _Rec(h) & " "
                    Next
                    _HostsList.Add(New HostList With {
                                   .IPAddress = _IP,
                                   .Hosts = _hString})
                End If
            Next
            If _HostsList IsNot Nothing Then
                Dim _iCt As Long = 0
                Using New WaitCursor
                    RefreshCache()
                    For Each h In _HostsList.Where(Function(x) x IsNot Nothing AndAlso Not x.IPAddress.Contains("#"))
                        _iCt += 1
                        InsertInto(h.IPAddress, h.Hosts)
                    Next
                    'refresh the datagrid
                    Dim _Data As IEnumerable(Of Typing) = Wrapper.GetResults(Of Typing)("Select ID, IPAddress, Hosts From LocalHostCache")
                    Me.LHC_DG.ItemsSource = _Data
                    _HostsList.Clear()
                End Using
            Else
                MsgBox("Odd.  Your hosts file does not contain anything, are you sure you picked the right file?  It's usually called 'hosts'...")
            End If
        Else
            MsgBox("Odd.  Your hosts file does not contain anything, are you sure you picked the right file?  It's usually called 'hosts'...")
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.Windows.RoutedEventArgs) Handles btnSave.Click
        Dim _IList As List(Of Typing) = LHC_DG.ItemsSource
        If _HostFilePath Is Nothing Then
            _HostFilePath = "C:\Windows\System32\drivers\etc\hosts"
        End If
        Using _sw As New IO.StreamWriter(_HostFilePath, False)
            Dim _lCt As Long = _IList.Count
            For i As Long = 0 To _lCt - 1
                _sw.WriteLine(_IList(i).IPAddress & " " & _IList(i).Hosts)
            Next
            _sw.Close()
        End Using
        MsgBox(_IList.Count & " Records Written")
    End Sub

    Private Sub RefreshCache()
        Wrapper.Execute("Delete From LocalHostCache")
    End Sub

    Private Sub DeleteRow(ByVal _ID As Long)
        Wrapper.Execute("Delete From LocalHostCache Where ID = @ID",
                        New Object() {"@ID"},
                        New Object() {_ID},
                        New DataType() {DataType.BigInt})
    End Sub

    Private Sub UpdateRow(ByVal _ID As Long, ByVal _IP As String, ByVal _HOSTS As String)
        Wrapper.Execute("Update LocalHostCache Set IPAddress = @IP, Hosts = @HOSTS Where ID = @ID",
                        New Object() {"@IP", "@HOSTS", "@ID"},
                        New Object() {_IP, _HOSTS, _ID},
                        New DataType() {DataType.NVarChar, DataType.NVarChar, DataType.BigInt})
    End Sub

    Private Sub InsertInto(ByVal _IP As String, ByVal _HOSTS As String)
        Wrapper.Execute("Insert Into LocalHostCache (IPAddress, Hosts) Values (@IP, @HOSTS)",
                        New Object() {"@IP", "@HOSTS"},
                        New Object() {_IP, _HOSTS},
                        New DataType() {DataType.NVarChar, DataType.NVarChar})
    End Sub

    Private Class HostList
        Public IPAddress As String
        Public Hosts As String
    End Class

End Class
